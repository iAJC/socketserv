import socket
import sys
import time


host = 'localhost'
port = 6969
size = 32000
s = None

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
except socket.error, (value, message):
    if s:
        s.close()
    print "Could not open socket: " + message
    sys.exit(1)
sendT = time.time()
s.send('Hello World')
data = s.recv(size)
endT = time.time()
talkT = endT - sendT
Kbs = (size/1000)/talkT
s.close()
print 'Recieved: ', data
print 'Client Speed:' + str(Kbs) + 'Kb/s'
