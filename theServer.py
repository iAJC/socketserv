'''
A socket server using threads that times reply and send times of loop
'''
import time
import socket
import sys

from thread import *
size = 32000

host = ''
port = 6969

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print 'Socket created, now binding socket to local host and port'

try:
    s.bind((host, port))
except socket.error, (value, message):
    if s:
        s.close()
    print "Couldn't open socket: " + str(value) + message
    sys.exit(1)

print "Socket bind Complete"

s.listen(10)
print "Socket now listening"


def clientthread(client):
    client.send('Welcome to my server, type something and hit enter!\n')

    while True:
        data = client.recv(32000)
        recieveT = time.time()
        reply = 'You sent me: ' + data + '\n'
        if not data:
            break

        client.sendall(reply)
        sentT = time.time()
        replyT = sentT - recieveT
        Kbs = (size/1000)/replyT
        client.send('Server speed is')
    client.close()


while 1:
    client, addr = s.accept()
    print "Connected with " + addr[0] + ':' + str(addr[1])

    start_new_thread(clientthread, (client,))

s.close()
